# Gary-Masinga-module-5

MTN Business App Academy Module 5 - Backend and Intelligence

1. Create a project on Firebase
2. Connect your web app to Firebase
3. Add a form to your app with at least 3 input fields
4. Write code to connect your app to the database to create, read, update and delete
