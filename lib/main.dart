import 'package:flutter/material.dart';
import 'package:sa_university_prospectus/pages/home.dart';
import 'package:firebase_core/firebase_core.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
      options: const FirebaseOptions(
          apiKey: "AIzaSyBByL_rcbM4-Ysfk21gD9lSRQ1rg6Lxk1g",
          authDomain: "flutterweb-82b71.firebaseapp.com",
          projectId: "flutterweb-82b71",
          storageBucket: "flutterweb-82b71.appspot.com",
          messagingSenderId: "382143976969",
          appId: "1:382143976969:web:884ada4b42b25f97824ad8"));
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'DataMate',
      theme: ThemeData(
          colorScheme: ColorScheme.fromSwatch(
        primarySwatch: Colors.blue,
      )),
      home: const HomePage(),
    );
  }
}
