import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: const Text("Contacts"),
          centerTitle: true,
        ),
        body: const NarrowLayout());
  }
}

class NarrowLayout extends StatefulWidget {
  const NarrowLayout({Key? key}) : super(key: key);

  @override
  State<NarrowLayout> createState() => _NarrowLayoutState();
}

class _NarrowLayoutState extends State<NarrowLayout> {
  final Stream<QuerySnapshot> _contacts =
      FirebaseFirestore.instance.collection("contacts").snapshots();

  @override
  Widget build(BuildContext context) {
    TextEditingController nameController = TextEditingController();
    TextEditingController surnameController = TextEditingController();
    TextEditingController phoneNumberController = TextEditingController();

    void _removeContact(id) {
      FirebaseFirestore.instance.collection("contacts").doc(id).delete();
    }

    void _editContact(data) {
      var con = FirebaseFirestore.instance.collection("contacts");
      TextEditingController _nameController = TextEditingController();
      TextEditingController _surnameController = TextEditingController();
      TextEditingController _phoneNumberController = TextEditingController();
      _nameController.text = data["ContactName"];
      _surnameController.text = data["ContactSurname"];
      _phoneNumberController.text = data["PhoneNumber"];
      showDialog(
          context: context,
          builder: (_) => AlertDialog(
                content: const Text('Update Contact information'),
                actions: [
                  TextField(
                    controller: _nameController,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  TextField(
                    controller: _surnameController,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  TextField(
                    controller: _phoneNumberController,
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  ElevatedButton(
                    onPressed: () {
                      con.doc(data['id']).update({
                        "ContactName": _nameController.text,
                        "ContactSurname": _surnameController.text,
                        "PhoneNumber": _phoneNumberController.text
                      });
                      Navigator.of(context).pop();
                    },
                    child: const Text("Save"),
                    style: ElevatedButton.styleFrom(
                        minimumSize: const Size(double.infinity, 60)),
                  ),
                ],
              ));
    }

    Future _addContact() {
      final name = nameController.text;
      final surname = surnameController.text;
      final phoneNumber = phoneNumberController.text;

      final firestoreRef =
          FirebaseFirestore.instance.collection("contacts").doc();
      return firestoreRef
          .set({
            "ContactName": name,
            "ContactSurname": surname,
            "PhoneNumber": phoneNumber,
            "id": firestoreRef.id
          })
          .then((value) => log("Contact added Successfully"))
          .catchError((onError) => log(onError));
    }

    Future<void> showAlertDialog(BuildContext context) async {
      return await showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              content: const Text(
                "Enter Contact Information",
                textAlign: TextAlign.center,
              ),
              actions: [
                TextField(
                  controller: nameController,
                  decoration: const InputDecoration(hintText: "Contact Name"),
                ),
                const SizedBox(
                  height: 10,
                ),
                TextField(
                  controller: surnameController,
                  decoration:
                      const InputDecoration(hintText: "Contact Surname"),
                ),
                const SizedBox(
                  height: 10,
                ),
                TextField(
                  controller: phoneNumberController,
                  decoration: const InputDecoration(hintText: "Phone Number"),
                ),
                const SizedBox(
                  height: 30,
                ),
                ElevatedButton(
                  onPressed: () {
                    _addContact();
                    Navigator.of(context).pop();
                  },
                  child: const Text("Save"),
                  style: ElevatedButton.styleFrom(
                      minimumSize: const Size(double.infinity, 60)),
                ),
              ],
            );
          });
    }

    return Stack(children: [
      const Center(
        child: Text(
          "Add your contacts",
          style: TextStyle(fontSize: 20, color: Colors.black),
        ),
      ),
      StreamBuilder(
        stream: _contacts,
        builder: (BuildContext context,
            AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
          if (snapshot.hasError) {
            return const Text("An error occurred when fetching data");
          }
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(child: CircularProgressIndicator());
          }
          if (snapshot.hasData) {
            return Column(
              children: [
                Expanded(
                  child: SizedBox(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height,
                    child: ListView(
                        children: snapshot.data!.docs
                            .map((DocumentSnapshot document) {
                      Map<String, dynamic> data =
                          document.data()! as Map<String, dynamic>;
                      return InkWell(
                        onTap: () {},
                        child: Card(
                          elevation: 10,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: ListTile(
                              title: Text(data['ContactName'] +
                                  " " +
                                  data['ContactSurname']),
                              subtitle: Text(data['PhoneNumber']),
                              trailing: IconButton(
                                onPressed: () {
                                  _removeContact(data['id']);
                                },
                                icon: const Icon(Icons.delete),
                              ),
                              leading: IconButton(
                                  onPressed: () {
                                    _editContact(data);
                                  },
                                  icon: const Icon(Icons.edit)),
                            ),
                          ),
                        ),
                      );
                    }).toList()),
                  ),
                ),
              ],
            );
          } else {
            return Center(
              child: Column(
                children: const [
                  Text(
                    "Add your contacts",
                    style: TextStyle(fontSize: 20, color: Colors.black),
                  ),
                ],
              ),
            );
          }
        },
      ),
      Padding(
        padding: const EdgeInsets.only(right: 20, bottom: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                FloatingActionButton(
                    child: const Icon(Icons.add),
                    onPressed: () {
                      showAlertDialog(context);
                    })
              ],
            )
          ],
        ),
      )
    ]);
  }
}
